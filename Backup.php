<?php

namespace Backup;
use Exception as BaseException;

/**
 * Class Exception
 * @package Backup
 */
class Exception extends BaseException
{}

/**
 * Class FTP
 * @package Backup
 */
class FTP
{
    const ERR_CONNECT     = 'FTP connection error';
    const ERR_LOGIN       = 'FTP login error';
    const ERR_CREATE_DIR  = 'Cannot create directory: %s';
    const ERR_REMOVE_DIR  = 'Cannot remove directory: %s';
    const ERR_REMOVE_FILE = 'Cannot delete file: %s';
    const ERR_UPLOADING   = 'There was a problem while uploading';

    private $path;
    private $host;
    private $user;
    private $password;

    /**
     * Setting config
     * @param array $ftpConfig
     */
    public function __construct(array $ftpConfig)
    {
        if ( count($ftpConfig) > 0 ) {
            foreach ($ftpConfig as $key => $value) {
                $this->{$key} = $value;
            }
        }
    }

    /**
     * Trying to connect to server
     *
     * @return resource
     * @throws Exception
     */
    public function connect()
    {
        static $connect;

        if ( empty($connect) ) {

            if ( !@$conn = ftp_connect($this->host) ) {
                throw new Exception( self::ERR_CONNECT );
            }

            if ( !@ftp_login($conn, $this->user, $this->password) ) {
                throw new Exception( self::ERR_LOGIN );
            }
            @ftp_pasv($conn, true);

            $connect = $conn;
        }

        return $connect;
    }

    /**
     * Getting ftp files list
     *
     * @return array
     */
    public function getFileList()
    {
        static $list;

        if ( empty($list) ) {
            $list = ftp_nlist($this->connect(), $this->path);
        }
        if (is_array($list)) {
            $key1 = array_search('.', $list);
            $key2 = array_search('..', $list);
            if ( isset($list[$key1]) ) {
                unset($list[$key1]);
            }
            if ( isset($list[$key2]) ) {
                unset($list[$key2]);
            }
        }

        return $list;
    }

    /**
     * Creating a directory
     *
     * @param string $dir
     * @throws Exception
     */
    public function makeDir($dir)
    {
        if ( !@ftp_mkdir($this->connect(), $dir) ) {
            throw new Exception( sprintf(self::ERR_CREATE_DIR, $dir) );
        }
    }

    /**
     * Removing a directory
     *
     * @param string $dir
     * @throws Exception
     */
    public function rmDir( $dir )
    {
        if ( !@ftp_rmdir($this->connect(), $dir) ) {
            throw new Exception( sprintf(self::ERR_REMOVE_DIR, $dir) );
        }
    }

    /**
     * Uploading file
     *
     * @param string $file
     * @throws Exception
     */
    public function uploadFile( $file )
    {
        if (!@ftp_put ( $this->connect(), $this->getPath() . '/' . basename($file), $file , FTP_BINARY)) {
            throw new Exception( self::ERR_UPLOADING );
        }
    }

    /**
     * Removing file
     *
     * @param string $file
     * @throws Exception
     */
    public function removeFile( $file )
    {
        if (!@ftp_delete ( $this->connect(), $file)) {
            throw new Exception( sprintf(self::ERR_REMOVE_FILE, $file));
        }
    }

    /**
     * Getting modification file time
     *
     * @param string $file
     * @return int
     */
    public function getFileModTime( $file )
    {
        return ftp_mdtm($this->connect(), $file);
    }

    /**
     * Getting ftp path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
}

/**
 * Class Backup
 * @package Backup
 */
class Backup
{
    const ERR_FILE_FOUND = 'File was not found: %s';
    const ERR_NO_FTP     = 'Set FTP before';
    const ERR_NO_FILE    = 'File cannot be empty';

    /** @var  FTP */
    private $ftp;
    /** @var string */
    private $file;
    /** @var  integer */
    private $maxFiles;

    /**
     * Setting max count of files on ftp
     *
     * @param integer $count
     */
    public function setMaxFiles( $count )
    {
        $this->maxFiles = $count;
    }

    /**
     * Setting FTP Class
     *
     * @param FTP $ftp
     */
    public function setFtp(FTP $ftp)
    {
        $this->ftp = $ftp;
    }

    /**
     * Setting file path, which you want upload
     *
     * @param string $path
     * @throws Exception
     */
    public function setFilePath($path)
    {
        if ( !file_exists($path) ) {
            throw new Exception( sprintf(self::ERR_FILE_FOUND, $path) );
        }

        $this->file = $path;
    }

    /**
     * Upload file to ftp
     *
     * @param FTP $fpt
     * @param string|null $filePath
     * @param integer|null $maxFiles
     * @throws Exception
     */
    public function process(FTP $fpt = null, $filePath = null, $maxFiles = null)
    {
        if ( $fpt != null ) {
            $this->setFtp($fpt);
        }
        if ( $filePath != null ) {
            $this->setFilePath($filePath);
        }
        if ( $maxFiles != null ) {
            $this->setMaxFiles($maxFiles);
        }

        if ( !$this->ftp instanceof FTP ) {
            throw new Exception( self::ERR_NO_FTP );
        }
        if ( $this->file == null ) {
            throw new Exception( self::ERR_NO_FILE );
        }

        $this->checkFileCount($maxFiles);
        $this->ftp->uploadFile( $this->file );
    }

    /**
     * Check count of files. Create dir, if not exist. Remove file if counts more than max
     */
    private function checkFileCount()
    {
        $files = $this->ftp->getFileList();
        if ( $files === false ) {
            //Create Dir
            $this->ftp->makeDir( $this->ftp->getPath() );
        } else if ($this->maxFiles != 0 && count($files) >= $this->maxFiles) {
            //Find latest file
            $latest =
                array(
                    'time' => time(),
                    'file' => null
                );

            foreach($files as $file) {
                $time = $this->ftp->getFileModTime($file);
                if ($time < $latest['time']) {
                    $latest['time'] = $time;
                    $latest['file'] = $file;
                }
            }
            //Remove latest file
            $this->ftp->removeFile($latest['file']);
        }
    }
}