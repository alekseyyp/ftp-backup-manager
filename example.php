<?php

include './Backup.php';

$params = array(
    'ftp' => array(
        'host'      => 'HOST',
        'user'      => 'USER',
        'password'  => 'PASSWORD',
        'path'      => 'test'
    ),
    'file_path' => __DIR__ . '/filename',
    'max_files' => 3
);

try {
    $ftp    = new \Backup\FTP( $params['ftp'] );
    $backup = new \Backup\Backup();

    $backup->process( $ftp, $params['file_path'], $params['max_files'] );

    die('File was uploaded successfully');
} catch (\Backup\Exception $e) {
    die($e->getMessage());
}